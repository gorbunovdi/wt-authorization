(function(){
	'use strict';

	angular.module('wt.authorization', []);
})();


(function(){
	'use strict';

	angular
		.module('wt.authorization')
		.directive('access', Access);

	Access.$inject = ['authorization'];

	function Access(authorizationProvider)
	{
		return {
			scope: {
				permission: '@',
				subject: '='
			},
			link: accessLink
		};

		function accessLink($scope, $element)
		{
			var authorizationService = authorizationProvider.getAuthorizationService();
			
			$scope.$watch('subject', authorize);
			authorize();

			function authorize()
			{
				var isGranted = authorizationService.isGranted($scope.permission, $scope.subject);

				if (true === isGranted)
				{
					$element.show();
				}
				else
				{
					$element.hide();
				}
			}
		}
	}
})();


(function(){
	'use strict';

	angular
		.module('wt.authorization')
		.provider('authorization', AuthorizationProvider)
	;

	function AuthorizationProvider()
	{
		var authorizationServiceName = '';

		this.setAuthorizationServiceName = function(serviceName)
		{
			return authorizationServiceName = serviceName;
		};

		this.$get = ['$injector', function($injector)
		{
			return {
				getAuthorizationService: getAuthorizationService
			};

			function getAuthorizationService()
			{
				if (authorizationServiceName.length === 0)
				{
					throw 'Authorization service name is not specified';
				}

				return $injector.get(authorizationServiceName);
			};
		}];
	}
})();

(function(){
	'use strict';

	angular
		.module('wt.authorization')
		.factory('Authorizer', Authorizer)
	;

	function Authorizer()
	{
		var service = {
			isAuthorized: isAuthorized
		};

		return service;

		function isAuthorized(user, permission, voter)
		{
			var isAllowed = false;

			permission.restrictions.map(function(restriction)
			{
				if(!isAllowed && isValidRestriction(restriction))
				{
					isAllowed = true;
				}
			});

			return isAllowed;

			function isValidRestriction(restriction)
			{
				var isValid = false;

				user.roles.map(function(role)
				{
					if (!isValid && role === restriction.role.role && isVerifiedRestriction(restriction))
					{
						isValid = true;
					}
				});

				return isValid;
			}

			function isVerifiedRestriction(restriction)
			{
				var isVerified = true;

				restriction.verifications.map(function(verification)
				{
					if (isVerified && !checkIsVerified(verification))
					{
						isVerified = false;
					}
				});

				return isVerified;
			}

			function checkIsVerified(verification)
			{
				return voter.verify(user, verification.type);
			}
		}
	}
})();

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImF1dGhvcml6YXRpb24ubW9kdWxlLmpzIiwiYWNjZXNzLmRpcmVjdGl2ZS5qcyIsImF1dGhvcml6YXRpb24ucHJvdmlkZXIuanMiLCJhdXRob3JpemVyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FDTkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQzNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJ3dC1hdXRob3JpemF0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCl7XG5cdCd1c2Ugc3RyaWN0JztcblxuXHRhbmd1bGFyLm1vZHVsZSgnd3QuYXV0aG9yaXphdGlvbicsIFtdKTtcbn0pKCk7XG5cbiIsIihmdW5jdGlvbigpe1xuXHQndXNlIHN0cmljdCc7XG5cblx0YW5ndWxhclxuXHRcdC5tb2R1bGUoJ3d0LmF1dGhvcml6YXRpb24nKVxuXHRcdC5kaXJlY3RpdmUoJ2FjY2VzcycsIEFjY2Vzcyk7XG5cblx0QWNjZXNzLiRpbmplY3QgPSBbJ2F1dGhvcml6YXRpb24nXTtcblxuXHRmdW5jdGlvbiBBY2Nlc3MoYXV0aG9yaXphdGlvblByb3ZpZGVyKVxuXHR7XG5cdFx0cmV0dXJuIHtcblx0XHRcdHNjb3BlOiB7XG5cdFx0XHRcdHBlcm1pc3Npb246ICdAJyxcblx0XHRcdFx0c3ViamVjdDogJz0nXG5cdFx0XHR9LFxuXHRcdFx0bGluazogYWNjZXNzTGlua1xuXHRcdH07XG5cblx0XHRmdW5jdGlvbiBhY2Nlc3NMaW5rKCRzY29wZSwgJGVsZW1lbnQpXG5cdFx0e1xuXHRcdFx0dmFyIGF1dGhvcml6YXRpb25TZXJ2aWNlID0gYXV0aG9yaXphdGlvblByb3ZpZGVyLmdldEF1dGhvcml6YXRpb25TZXJ2aWNlKCk7XG5cdFx0XHRcblx0XHRcdCRzY29wZS4kd2F0Y2goJ3N1YmplY3QnLCBhdXRob3JpemUpO1xuXHRcdFx0YXV0aG9yaXplKCk7XG5cblx0XHRcdGZ1bmN0aW9uIGF1dGhvcml6ZSgpXG5cdFx0XHR7XG5cdFx0XHRcdHZhciBpc0dyYW50ZWQgPSBhdXRob3JpemF0aW9uU2VydmljZS5pc0dyYW50ZWQoJHNjb3BlLnBlcm1pc3Npb24sICRzY29wZS5zdWJqZWN0KTtcblxuXHRcdFx0XHRpZiAodHJ1ZSA9PT0gaXNHcmFudGVkKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0JGVsZW1lbnQuc2hvdygpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdGVsc2Vcblx0XHRcdFx0e1xuXHRcdFx0XHRcdCRlbGVtZW50LmhpZGUoKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH1cblx0fVxufSkoKTtcblxuIiwiKGZ1bmN0aW9uKCl7XG5cdCd1c2Ugc3RyaWN0JztcblxuXHRhbmd1bGFyXG5cdFx0Lm1vZHVsZSgnd3QuYXV0aG9yaXphdGlvbicpXG5cdFx0LnByb3ZpZGVyKCdhdXRob3JpemF0aW9uJywgQXV0aG9yaXphdGlvblByb3ZpZGVyKVxuXHQ7XG5cblx0ZnVuY3Rpb24gQXV0aG9yaXphdGlvblByb3ZpZGVyKClcblx0e1xuXHRcdHZhciBhdXRob3JpemF0aW9uU2VydmljZU5hbWUgPSAnJztcblxuXHRcdHRoaXMuc2V0QXV0aG9yaXphdGlvblNlcnZpY2VOYW1lID0gZnVuY3Rpb24oc2VydmljZU5hbWUpXG5cdFx0e1xuXHRcdFx0cmV0dXJuIGF1dGhvcml6YXRpb25TZXJ2aWNlTmFtZSA9IHNlcnZpY2VOYW1lO1xuXHRcdH07XG5cblx0XHR0aGlzLiRnZXQgPSBbJyRpbmplY3RvcicsIGZ1bmN0aW9uKCRpbmplY3Rvcilcblx0XHR7XG5cdFx0XHRyZXR1cm4ge1xuXHRcdFx0XHRnZXRBdXRob3JpemF0aW9uU2VydmljZTogZ2V0QXV0aG9yaXphdGlvblNlcnZpY2Vcblx0XHRcdH07XG5cblx0XHRcdGZ1bmN0aW9uIGdldEF1dGhvcml6YXRpb25TZXJ2aWNlKClcblx0XHRcdHtcblx0XHRcdFx0aWYgKGF1dGhvcml6YXRpb25TZXJ2aWNlTmFtZS5sZW5ndGggPT09IDApXG5cdFx0XHRcdHtcblx0XHRcdFx0XHR0aHJvdyAnQXV0aG9yaXphdGlvbiBzZXJ2aWNlIG5hbWUgaXMgbm90IHNwZWNpZmllZCc7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRyZXR1cm4gJGluamVjdG9yLmdldChhdXRob3JpemF0aW9uU2VydmljZU5hbWUpO1xuXHRcdFx0fTtcblx0XHR9XTtcblx0fVxufSkoKTtcbiIsIihmdW5jdGlvbigpe1xuXHQndXNlIHN0cmljdCc7XG5cblx0YW5ndWxhclxuXHRcdC5tb2R1bGUoJ3d0LmF1dGhvcml6YXRpb24nKVxuXHRcdC5mYWN0b3J5KCdBdXRob3JpemVyJywgQXV0aG9yaXplcilcblx0O1xuXG5cdGZ1bmN0aW9uIEF1dGhvcml6ZXIoKVxuXHR7XG5cdFx0dmFyIHNlcnZpY2UgPSB7XG5cdFx0XHRpc0F1dGhvcml6ZWQ6IGlzQXV0aG9yaXplZFxuXHRcdH07XG5cblx0XHRyZXR1cm4gc2VydmljZTtcblxuXHRcdGZ1bmN0aW9uIGlzQXV0aG9yaXplZCh1c2VyLCBwZXJtaXNzaW9uLCB2b3Rlcilcblx0XHR7XG5cdFx0XHR2YXIgaXNBbGxvd2VkID0gZmFsc2U7XG5cblx0XHRcdHBlcm1pc3Npb24ucmVzdHJpY3Rpb25zLm1hcChmdW5jdGlvbihyZXN0cmljdGlvbilcblx0XHRcdHtcblx0XHRcdFx0aWYoIWlzQWxsb3dlZCAmJiBpc1ZhbGlkUmVzdHJpY3Rpb24ocmVzdHJpY3Rpb24pKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aXNBbGxvd2VkID0gdHJ1ZTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cblx0XHRcdHJldHVybiBpc0FsbG93ZWQ7XG5cblx0XHRcdGZ1bmN0aW9uIGlzVmFsaWRSZXN0cmljdGlvbihyZXN0cmljdGlvbilcblx0XHRcdHtcblx0XHRcdFx0dmFyIGlzVmFsaWQgPSBmYWxzZTtcblxuXHRcdFx0XHR1c2VyLnJvbGVzLm1hcChmdW5jdGlvbihyb2xlKVxuXHRcdFx0XHR7XG5cdFx0XHRcdFx0aWYgKCFpc1ZhbGlkICYmIHJvbGUgPT09IHJlc3RyaWN0aW9uLnJvbGUucm9sZSAmJiBpc1ZlcmlmaWVkUmVzdHJpY3Rpb24ocmVzdHJpY3Rpb24pKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdGlzVmFsaWQgPSB0cnVlO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSk7XG5cblx0XHRcdFx0cmV0dXJuIGlzVmFsaWQ7XG5cdFx0XHR9XG5cblx0XHRcdGZ1bmN0aW9uIGlzVmVyaWZpZWRSZXN0cmljdGlvbihyZXN0cmljdGlvbilcblx0XHRcdHtcblx0XHRcdFx0dmFyIGlzVmVyaWZpZWQgPSB0cnVlO1xuXG5cdFx0XHRcdHJlc3RyaWN0aW9uLnZlcmlmaWNhdGlvbnMubWFwKGZ1bmN0aW9uKHZlcmlmaWNhdGlvbilcblx0XHRcdFx0e1xuXHRcdFx0XHRcdGlmIChpc1ZlcmlmaWVkICYmICFjaGVja0lzVmVyaWZpZWQodmVyaWZpY2F0aW9uKSlcblx0XHRcdFx0XHR7XG5cdFx0XHRcdFx0XHRpc1ZlcmlmaWVkID0gZmFsc2U7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9KTtcblxuXHRcdFx0XHRyZXR1cm4gaXNWZXJpZmllZDtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gY2hlY2tJc1ZlcmlmaWVkKHZlcmlmaWNhdGlvbilcblx0XHRcdHtcblx0XHRcdFx0cmV0dXJuIHZvdGVyLnZlcmlmeSh1c2VyLCB2ZXJpZmljYXRpb24udHlwZSk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG59KSgpO1xuIl0sInNvdXJjZVJvb3QiOiIvc291cmNlLyJ9

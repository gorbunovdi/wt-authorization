var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('concat', function() {
	return gulp.src([
		'src/*.module.js',
		'src/*.js'
	])
		.pipe(sourcemaps.init())
			.pipe(concat('wt-authorization.js'))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('dist'))
	;
});

gulp.task('watch', function() {
	gulp.watch('src/*.js', ['concat']);
});

gulp.task('default', ['concat', 'watch']);

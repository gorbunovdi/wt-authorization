(function(){
	'use strict';

	angular
		.module('wt.authorization')
		.factory('Authorizer', Authorizer)
	;

	function Authorizer()
	{
		var service = {
			isAuthorized: isAuthorized
		};

		return service;

		function isAuthorized(user, permission, voter)
		{
			var isAllowed = false;

			permission.restrictions.map(function(restriction)
			{
				if(!isAllowed && isValidRestriction(restriction))
				{
					isAllowed = true;
				}
			});

			return isAllowed;

			function isValidRestriction(restriction)
			{
				var isValid = false;

				user.roles.map(function(role)
				{
					if (!isValid && role === restriction.role.role && isVerifiedRestriction(restriction))
					{
						isValid = true;
					}
				});

				return isValid;
			}

			function isVerifiedRestriction(restriction)
			{
				var isVerified = true;

				restriction.verifications.map(function(verification)
				{
					if (isVerified && !checkIsVerified(verification))
					{
						isVerified = false;
					}
				});

				return isVerified;
			}

			function checkIsVerified(verification)
			{
				return voter.verify(user, verification.type);
			}
		}
	}
})();

(function(){
	'use strict';

	angular
		.module('wt.authorization')
		.provider('authorization', AuthorizationProvider)
	;

	function AuthorizationProvider()
	{
		var authorizationServiceName = '';

		this.setAuthorizationServiceName = function(serviceName)
		{
			return authorizationServiceName = serviceName;
		};

		this.$get = ['$injector', function($injector)
		{
			return {
				getAuthorizationService: getAuthorizationService
			};

			function getAuthorizationService()
			{
				if (authorizationServiceName.length === 0)
				{
					throw 'Authorization service name is not specified';
				}

				return $injector.get(authorizationServiceName);
			};
		}];
	}
})();

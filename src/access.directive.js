(function(){
	'use strict';

	angular
		.module('wt.authorization')
		.directive('access', Access);

	Access.$inject = ['authorization'];

	function Access(authorizationProvider)
	{
		return {
			scope: {
				permission: '@',
				subject: '='
			},
			link: accessLink
		};

		function accessLink($scope, $element)
		{
			var authorizationService = authorizationProvider.getAuthorizationService();
			
			$scope.$watch('subject', authorize);
			authorize();

			function authorize()
			{
				var isGranted = authorizationService.isGranted($scope.permission, $scope.subject);

				if (true === isGranted)
				{
					$element.show();
				}
				else
				{
					$element.hide();
				}
			}
		}
	}
})();

